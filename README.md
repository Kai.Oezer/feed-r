<img alt="Feed-r" style="border-radius:36px" src="Feedr/Images.xcassets/AppIcon.appiconset/feedr_icon_180x180.png"/>

# Feed-r

This repository contains the updated version of an RSS reader app for iOS that I created in 2014, shortly after Apple released its new programming language Swift. The app became available in the App Store with the release of iOS 8. The app was pulled a few months later when my Apple Developer account expired, which I didn't bother renewing at that time.

In late 2017, I renamed the app to "Aressess" (_= R-S-S_) and released the source code on [GitHub](https://github.com/robo-fish/Aressess).

In mid 2021, I copied the sources to this new GitLab repository, reverting the name to Feed-r. The code is updated for Swift 5 and the iOS 14 / macOS 11 APIs.

# Dependencies

## FXKit

Feed-r uses the [FXKit](https://gitlab.com/Kai.Oezer/FXKit) framework. The most convenient way of working with the Feed-r code is to use the included __Xcode workspace__ which contains both the FXKit project and the Feed-r project. Feed-r builds can thus access FXKit build results without having to tinker with framework search paths in the build settings.
